#!/usr/bin/env python

import numpy as np
import matplotlib
matplotlib.use('tkagg')
import matplotlib.pyplot as plt
import elphmod

data = np.loadtxt('bands.dat.gp')

plt.plot(data[:, 0], data[:, 1:], color='red')
plt.xlabel('wave vector ($2 \pi / a$)')
plt.ylabel('phonon frequency ($\mathrm{cm}^{-1}$)')
plt.show()
