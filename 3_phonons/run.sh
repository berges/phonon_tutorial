#!/bin/bash

. ../environment.sh

# 1. Perform DFT calculation:

mpirun -np $NP pw.x -nk $NK < 1_pw.in | tee 1_pw.out

# 2. Perform DFPT calculation:

mpirun -np $NP ph.x -nk $NK < 2_ph.in | tee 2_ph.out

# 3. Calculate interatomic force constants from dynamical matrices:

q2r.x < 3_q2r.in | tee 3_q2r.out

# 4. Calculate phonon dispersion from interatomic force constants:

matdyn.x < 4_matdyn.in | tee 4_matdyn.out

# 5. Show plotted phonon dispersion:

$CONDA/python 5_plot.py

# 6. Calculate and show dispersion and DOS from dynamical matrix:

$CONDA/mpirun -np $NP $CONDA/python -u 6_plot.py
