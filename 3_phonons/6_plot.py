#!/usr/bin/env python

import numpy as np
import matplotlib
matplotlib.use('tkagg')
import matplotlib.pyplot as plt
import elphmod

comm = elphmod.MPI.comm
info = elphmod.MPI.info

Ry2eV = 13.605693009

info("Set up dynamical matrix..")

model = elphmod.ph.model('ifc', apply_asr=True)
D = elphmod.ph.dynamical_matrix(*model)

info("Diagonalize dynamical matrix along G-M-K-G..")

q, x, GMKG = elphmod.bravais.GMKG(corner_indices=True)
w2, u, order = elphmod.dispersion.dispersion(D, q, vectors=True, order=True)
w = elphmod.ph.sgnsqrt(w2) * Ry2eV * 1e3

info("Diagonalize dynamical matrix on uniform mesh..")

w2_full, order = elphmod.dispersion.dispersion_full(D, 60, order=True)
w_full = elphmod.ph.sgnsqrt(w2_full) * Ry2eV * 1e3

info("Calculate DOS..")

e = np.linspace(w_full.min(), w_full.max(), 300)

DOS = 0
for nu in range(D.size):
    DOS = DOS + elphmod.dos.hexDOS(w_full[:, :, nu])(e)

info("Plot dispersion and DOS..")

if comm.rank == 0:
    fig, (ax1, ax2) = plt.subplots(1, 2, sharey=True)

    ax1.set_ylabel('phonon frequency (meV)')
    ax1.set_xlabel('wave vector')
    ax2.set_xlabel('density of states (1/meV)')

    ax1.set_xticks(x[GMKG])
    ax1.set_xticklabels('GMKG')

    polarization = elphmod.ph.polarization(u, q)

    colors = ['skyblue', 'dodgerblue', 'orange']

    for nu in range(D.size):
        X, Y = elphmod.plot.compline(x, w[:, nu], 0.5 * polarization[:, nu])

        for i in range(3):
            ax1.fill(X, Y[i], color=colors[i], linewidth=0.0)

    ax2.fill(DOS, e, color='gray')

    plt.show()
