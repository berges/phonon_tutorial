#!/bin/bash

# adapted from Samuel Ponce's pp.py

SOURCE=../3_phonons
TARGET=save
PREFIX=TaS2
POINTS=2

mkdir -p $TARGET

cp -r $SOURCE/work/_ph0/$PREFIX.phsave $TARGET

for POINT in `seq $POINTS`
do
    cp $SOURCE/dyn$POINT $TARGET/$PREFIX.dyn_q$POINT

    COMMAND="cp $SOURCE/work/_ph0"

    test $POINT -gt 1 && COMMAND="$COMMAND/$PREFIX.q_$POINT"

    COMMAND="$COMMAND/$PREFIX.dvscf1 $TARGET/$PREFIX.dvscf_q$POINT"

    $COMMAND
done
