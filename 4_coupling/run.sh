#!/bin/bash

. ../environment.sh

NP=19  # number of processes
NK=$NP # number of k-point pools

# 1. Provide EPW with required data from "ph.x":

./1_ph2epw.sh

# 2. Perform self-consistent (SCF) DFT calculation:

mpirun -np $NP pw.x -nk $NK < 2_scf.in | tee 2_scf.out

# 3. Perform non-SCF DFT calculation:

mpirun -np $NP pw.x -nk $NK < 3_nscf.in | tee 3_nscf.out

# 4. Perform EPW calculation:

mpirun -np $NP epw.x -nk $NK < 4_epw.in > 4_epw.out

# 5. Show plotted DFT and Wannier bands:

$CONDA/python 5_plot.py

# 6. Show strengh of el.-ph. coupling along phonon bands:

$CONDA/python 6_plot.py
