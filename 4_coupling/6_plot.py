#!/usr/bin/env python

import numpy as np
import matplotlib
matplotlib.use('tkagg')
import matplotlib.pyplot as plt
import elphmod

comm = elphmod.MPI.comm
info = elphmod.MPI.info

Ry2eV = 13.605693009

# read coupling from EPW's standard output (prtgkk = .true.)
# or perform back-transform externally?

from_epw_stdout = False

nk = 12 # k points per dimension (coarse/ph.x mesh)
nq = 2  # q points per dimension (coarse/ph.x mesh)
Nk = 12 # k points per dimension (fine/epw.x mesh)
Nq = 12 # q points per dimension (fine/epw.x mesh)

info("Set up dynamical matrix..")

phid, amass, at, tau = elphmod.ph.model('../3_phonons/ifc', apply_asr=True)
D = elphmod.ph.dynamical_matrix(phid, amass, at, tau)

info("Diagonalize dynamical matrix along G-M-K-G..")

q, x, GMKG = elphmod.bravais.GMKG(Nq, mesh=True, corner_indices=True)
q_int = np.round(q * Nq / (2 * np.pi)).astype(int)

w2 = elphmod.dispersion.dispersion(D, q)
w = elphmod.ph.sgnsqrt(w2) * Ry2eV

if from_epw_stdout:
    info("Read el.-ph. coupling from EPW's standard output..")

    g = elphmod.elph.read_EPW_output('4_epw.out', q=q_int[:-1], nq=Nq,
        nb=D.size, nk=Nk, bands=1, squeeze=True, status=False)

    g = np.concatenate((g, g[:1]), axis=0)

    for iq in range(len(q)):
        for nu in range(D.size):
            g[iq, nu] *= np.sqrt(2 * abs(w[iq, nu]))
else:
    info("Construct Wigner-Seitz cells..")

    elphmod.bravais.write_wigner_file(name='wigner.dat', nk=nk, nq=nq,
        at=at, tau=tau)

    info("Emulate EPW back-transform..")

    elphmod.elph.epw(epmatwp='work/TaS2.epmatwp1', wigner='wigner.dat',
        outdir='.', nbndsub=3, nmodes=D.size, nk=Nk, nq=Nq,
        orbital_basis=False, wannier='TaS2_hr.dat',
        displacement_basis=False, ifc='../3_phonons/ifc')

    info("Read transformed el.-ph. coupling..")

    q_irr = sorted(elphmod.bravais.irreducibles(Nq))
    q_num = np.array([q_irr.index((q1, q2)) for q1, q2 in q_int]) + 1

    g = elphmod.elph.coupling('el-ph-%d.dat', bands=3, offset=0, nQ=None,
        Q=q_num, nb=D.size, nk=Nk, squeeze=True, status=True, completion=False,
        phase=True)[..., 0, 0, :, :]

    g = np.absolute(g) * Ry2eV ** 1.5

info("Take average el.-ph. coupling and scale it to plot..")

g = g.sum(axis=-1).sum(axis=-1)

info("Plot dispersion and DOS..")

if comm.rank == 0:
    plt.ylabel('phonon frequency (meV)')
    plt.xlabel('wave vector')

    plt.xticks(x[GMKG], 'GMKG')

    for nu in range(D.size):
        plt.errorbar(x, w[:, nu] * 1e3, g[:, nu], color='blue')

    plt.show()
