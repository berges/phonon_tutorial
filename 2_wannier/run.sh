#!/bin/bash

. ../environment.sh

# 1. Perform self-consistent (SCF) DFT calculation:

mpirun -np $NP pw.x -nk $NK < 1_scf.in | tee 1_scf.out

# 2. Perform non-SCF DFT calculation:

mpirun -np $NP pw.x -nk $NK < 2_nscf.in | tee 2_nscf.out

# 3. Ask Wannier90 about required data:

wannier90.x -pp TaS2

# 4. Provide Wannier90 with required data from Quantum ESPRESSO:

mpirun -np $NP pw2wannier90.x < 4_pw2w90.in | tee 4_pw2w90.out

# 5. Calculate Wannier Hamiltonian:

wannier90.x TaS2

# 6. Show plotted DFT and Wannier bands:

$CONDA/python 6_plot.py

# 7. Calculate and show Wannier bands and DOS from Hamiltonian:

$CONDA/mpirun -np $NP $CONDA/python -u 7_plot.py
