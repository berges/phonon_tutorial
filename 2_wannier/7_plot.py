#!/usr/bin/env python

import numpy as np
import matplotlib
matplotlib.use('tkagg')
import matplotlib.pyplot as plt
import elphmod

comm = elphmod.MPI.comm
info = elphmod.MPI.info

info("Read Fermi level from SCF output file..")

mu = elphmod.el.read_Fermi_level('1_scf.out')

info("Set up Wannier Hamiltonian..")

H = elphmod.el.hamiltonian('TaS2_hr.dat')

info("Diagonalize Hamiltonian along G-M-K-G..")

k, x, GMKG = elphmod.bravais.GMKG(corner_indices=True)
eps, psi, order = elphmod.dispersion.dispersion(H, k, vectors=True, order=True)
eps -= mu

info("Diagonalize Hamiltonian on uniform mesh..")

eps_full = elphmod.dispersion.dispersion_full(H, 120) - mu

info("Calculate DOS of metallic band..")

e = np.linspace(eps_full.min(), eps_full.max(), 300)
DOS = elphmod.dos.hexDOS(eps_full[:, :, 0])(e)

info("Plot dispersion and DOS..")

if comm.rank == 0:
    fig, (ax1, ax2) = plt.subplots(1, 2, sharey=True)

    ax1.set_ylabel('electron energy (eV)')
    ax1.set_xlabel('wave vector')
    ax2.set_xlabel('density of states (1/eV)')

    ax1.set_xticks(x[GMKG])
    ax1.set_xticklabels('GMKG')

    for n in range(H.size):
        X, Y = elphmod.plot.compline(x, eps[:, n],
            0.05 * (psi[:, :, n] * psi[:, :, n].conj()).real)

        for i in range(3):
            ax1.fill(X, Y[i], color='RCB'[i])

    ax2.fill(DOS, e, color='gray')

    plt.show()
