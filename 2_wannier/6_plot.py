#!/usr/bin/env python

import numpy as np
import matplotlib
matplotlib.use('tkagg')
import matplotlib.pyplot as plt
import elphmod

k, e = elphmod.el.read_bands_plot('../1_electrons/bands.dat.gnu', bands=13)
K, E = elphmod.el.read_bands_plot('TaS2_band.dat', bands=3)

K *= k[-1] / K[-1]

plt.plot(k, e, 'kx')
plt.plot(K, E, 'r')
plt.xlabel('wave vector ($2 \pi / a$)')
plt.ylabel('electron energy (eV)')
plt.show()
