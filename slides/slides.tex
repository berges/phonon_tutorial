\documentclass[aspectratio=169, serif]{beamer}

\setbeamertemplate{navigation symbols}{}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}

\usepackage{concmath}
\usepackage[T1]{fontenc}

\pdfpkresolution=2400
\pdfpkmode={supre}

\usefonttheme{professionalfonts}

\usepackage{mathtools, graphicx, tikz, bm, upgreek, listings}

\definecolor{1}{RGB}{233, 89, 37}
\definecolor{2}{RGB}{224, 0, 26}
\definecolor{3}{RGB}{176, 15, 30}

\usecolortheme[named=2]{structure}

\title{Phonons and electron-phonon interaction\\
  \textcolor{1}{(of H-TaS$\sub2$)} from Quantum ESPRESSO}
\author{Jan Berges, Tim Wehling, Christof Köhler}
\date{7 January 2019}

\let\vec\bm
\let\epsilon\varepsilon

\def\D{\mathrm d}
\def\E{\mathrm e}
\def\I{\mathrm i}

\def\sub#1{\sb{\text{#1}}}
\def\super#1{\sp{\text{#1}}}

\def\bra#1{\langle#1|}
\def\ket#1{|#1\rangle}
\def\bracket#1#2{\langle#1|#2\rangle}
\def\av#1{\langle#1\rangle}

\lstset{
    basicstyle = \color{2} \scriptsize,
    keywordstyle = \color{3},
    identifierstyle = \color{black},
    commentstyle = \color{1},
    showstringspaces = false,
    }

\def\popup#1{%
  \begin{tikzpicture}[remember picture, overlay]
    \uncover<2>{
      \fill [gray, fill opacity=0.4] (current page.south west) rectangle (current page.north east);
      \node [draw, fill=white, rounded corners, inner sep=2mm, align=center] at (current page.center)
        { \includegraphics[width=0.7\textwidth]{#1} };
      }
  \end{tikzpicture}
  }

\begin{document}
  \begin{frame}
    \thispagestyle{empty}
    \maketitle
    \begin{tikzpicture}[remember picture, overlay]
      \node [above=-1pt, fill=1!30] at (current page.south) {
        \begin{minipage}{\paperwidth}
          \centering%
          \hspace*{2mm}%
          \includegraphics[height=5mm]{fig/uni.pdf}%
          \hfill%
          \raisebox{1ex}{\it RTG QM3 --- Hands-on tutorial}%
          \hfill%
          \includegraphics[height=5mm]{fig/dfg.pdf}%
          \hspace*{2mm}%
        \end{minipage}
        };
    \end{tikzpicture}
  \end{frame}

  \begin{frame}{Structure}
    \tableofcontents
    \begin{tikzpicture}[remember picture, overlay]
      \node [below left=5mm] at (current page.north east)
        {\includegraphics[width=6cm]{fig/qe.pdf}};
    \end{tikzpicture}
  \end{frame}

  \section{Prerequisites}

  \begin{frame}{Prerequisites}
    \begin{itemize}
      \item ssh -X qm3rtg@qm3-login.bccms.uni-bremen.de
      \item ssh -X node\textit{\color1XXX}
      \item cd /scratch/qm3rtg
      \item git clone https://berges@bitbucket.org/berges/phonon\_tutorial.git \textit{\color1UserXXX}
      \item cd \textit{\color1UserXXX}/1\_electrons
      \item ./run.sh
      \item \dots
    \end{itemize}
  \end{frame}

  \section{\protect\textit{pw.x} --- Electrons dispersion from DFT}

  \begin{frame}{\textit{pw.x} --- Electrons dispersion from DFT}
    {$\blacktriangleright$ mpirun -np 20 pw.x -nk 10 < 1\_scf.in | tee 1\_scf.out}
    \begin{minipage}{0.4\textwidth}
      \lstinputlisting[lastline=18]{../1_electrons/1_scf.in}
    \end{minipage}%
    \begin{minipage}{0.6\textwidth}
      \lstinputlisting[firstline=19]{../1_electrons/1_scf.in}
    \end{minipage}%
  \end{frame}

  \begin{frame}{\textit{pw.x} --- Electrons dispersion from DFT}
    {$\blacktriangleright$ mpirun -np 20 pw.x -nk 10 < 2\_nscf.in | tee 2\_nscf.out}
    \begin{minipage}{0.4\textwidth}
      \lstinputlisting[lastline=18]{../1_electrons/2_nscf.in}
    \end{minipage}%
    \begin{minipage}{0.6\textwidth}
      \lstinputlisting[firstline=19, lastline=37]{../1_electrons/2_nscf.in}
    \end{minipage}%
  \end{frame}

  \begin{frame}{\textit{pw.x} --- Electrons dispersion from DFT}
    {$\blacktriangleright$ mpirun -np 20 bands.x -nk 10 < 3\_bands.in | tee 3\_bands.out}
    \lstinputlisting{../1_electrons/3_bands.in}
  \end{frame}

  \begin{frame}{\textit{pw.x} --- Electrons dispersion from DFT}
    {$\blacktriangleright$ python 4\_plot.py}
    \lstinputlisting[language=python]{../1_electrons/4_plot.py}
    \popup{fig/1-4.pdf}
  \end{frame}

  \section{\protect\textit{wannier90.x} --- Wannier interpolation}

  \begin{frame}{\textit{wannier90.x} --- Wannier interpolation}
    {$\blacktriangleright$ mpirun -np 20 pw.x -nk 10 < 1\_scf.in | tee 1\_scf.out}
    \begin{minipage}{0.4\textwidth}
      \lstinputlisting[lastline=18]{../2_wannier/1_scf.in}
    \end{minipage}%
    \begin{minipage}{0.6\textwidth}
      \lstinputlisting[firstline=19]{../2_wannier/1_scf.in}
    \end{minipage}%
  \end{frame}

  \begin{frame}{\textit{wannier90.x} --- Wannier interpolation}
    {$\blacktriangleright$ mpirun -np 20 pw.x -nk 10 < 2\_nscf.in | tee 2\_nscf.out}
    \begin{minipage}{0.4\textwidth}
      \lstinputlisting[lastline=18]{../2_wannier/2_nscf.in}
    \end{minipage}%
    \begin{minipage}{0.6\textwidth}
      \lstinputlisting[firstline=19, lastline=37]{../2_wannier/2_nscf.in}
    \end{minipage}%
  \end{frame}

  \begin{frame}{\textit{wannier90.x} --- Wannier interpolation}
    {$\blacktriangleright$ wannier90.x -pp TaS2}
    \begin{minipage}{0.4\textwidth}
      \lstinputlisting[lastline=18]{../2_wannier/TaS2.win}
    \end{minipage}%
    \begin{minipage}{0.6\textwidth}
      \lstinputlisting[firstline=19, lastline=29]{../2_wannier/TaS2.win}
    \end{minipage}%
  \end{frame}

  \begin{frame}{\textit{wannier90.x} --- Wannier interpolation}
    {$\blacktriangleright$ mpirun -np 20 pw2wannier90.x < 4\_pw2w90.in | tee 4\_pw2w90.out}
    \lstinputlisting{../2_wannier/4_pw2w90.in}
  \end{frame}

  \begin{frame}{\textit{wannier90.x} --- Wannier interpolation}
    {$\blacktriangleright$ wannier90.x TaS2}
    \begin{minipage}{0.4\textwidth}
      \lstinputlisting[lastline=18]{../2_wannier/TaS2.win}
    \end{minipage}%
    \begin{minipage}{0.6\textwidth}
      \lstinputlisting[firstline=19, lastline=29]{../2_wannier/TaS2.win}
    \end{minipage}%
  \end{frame}

  \begin{frame}{\textit{wannier90.x} --- Wannier interpolation}
    {$\blacktriangleright$ python 6\_plot.py}
    \lstinputlisting[language=python]{../2_wannier/6_plot.py}
    \popup{fig/2-6.pdf}
  \end{frame}

  \begin{frame}{\textit{wannier90.x} --- Wannier interpolation}
    {$\blacktriangleright$ mpirun -np 20 python -u 7\_plot.py}
    \lstinputlisting[language=python, lastline=21]{../2_wannier/7_plot.py}
    \popup{fig/2-7.pdf}
  \end{frame}

  \section{\protect\textit{ph.x} --- Phonon dispersion from DFPT}

  \begin{frame}{\textit{ph.x} --- Phonons dispersion from DFPT}
    {$\blacktriangleright$ mpirun -np 20 pw.x -nk 10 < 1\_pw.in | tee 1\_pw.out}
    \begin{minipage}{0.4\textwidth}
      \lstinputlisting[lastline=18]{../3_phonons/1_pw.in}
    \end{minipage}%
    \begin{minipage}{0.6\textwidth}
      \lstinputlisting[firstline=19]{../3_phonons/1_pw.in}
    \end{minipage}%
  \end{frame}

  \begin{frame}{\textit{ph.x} --- Phonons dispersion from DFPT}
    {$\blacktriangleright$ mpirun -np 20 ph.x -nk 10 < 2\_ph.in | tee 2\_ph.out}
    \lstinputlisting{../3_phonons/2_ph.in}
  \end{frame}

  \begin{frame}{\textit{ph.x} --- Phonons dispersion from DFPT}
    {$\blacktriangleright$ q2r.x < 3\_q2r.in | tee 3\_q2r.out}
    \lstinputlisting{../3_phonons/3_q2r.in}
  \end{frame}

  \begin{frame}{\textit{ph.x} --- Phonons dispersion from DFPT}
    {$\blacktriangleright$ matdyn.x < 4\_matdyn.in | tee 4\_matdyn.out}
    \lstinputlisting{../3_phonons/4_matdyn.in}
  \end{frame}

  \begin{frame}{\textit{ph.x} --- Phonons dispersion from DFPT}
    {$\blacktriangleright$ python 5\_plot.py}
    \lstinputlisting[language=python]{../3_phonons/5_plot.py}
    \popup{fig/3-5.pdf}
  \end{frame}

  \begin{frame}{\textit{ph.x} --- Phonons dispersion from DFPT}
    {$\blacktriangleright$ mpirun -np 20 python -u 6\_plot.py}
    \lstinputlisting[language=python, lastline=21]{../3_phonons/6_plot.py}
    \popup{fig/3-6.pdf}
  \end{frame}

  \section{\protect\textit{epw.x} --- Electron-phonon coupling using Wannier functions}

  \begin{frame}{\textit{epw.x} --- Electron-phonon coupling using Wannier functions}
    {$\blacktriangleright$ ./1\_ph2epw.sh}
      \lstinputlisting[firstline=5, language=bash]{../4_coupling/1_ph2epw.sh}
  \end{frame}

  \begin{frame}{\textit{epw.x} --- Electron-phonon coupling using Wannier functions}
    {$\blacktriangleright$ mpirun -np 20 pw.x -nk 10 < 2\_scf.in | tee 2\_scf.out}
    \begin{minipage}{0.4\textwidth}
      \lstinputlisting[lastline=18]{../4_coupling/2_scf.in}
    \end{minipage}%
    \begin{minipage}{0.6\textwidth}
      \lstinputlisting[firstline=19]{../4_coupling/2_scf.in}
    \end{minipage}%
  \end{frame}

  \begin{frame}{\textit{epw.x} --- Electron-phonon coupling using Wannier functions}
    {$\blacktriangleright$ mpirun -np 20 pw.x -nk 10 < 3\_nscf.in | tee 3\_nscf.out}
    \begin{minipage}{0.4\textwidth}
      \lstinputlisting[lastline=18]{../4_coupling/3_nscf.in}
    \end{minipage}%
    \begin{minipage}{0.6\textwidth}
      \lstinputlisting[firstline=19, lastline=38]{../4_coupling/3_nscf.in}
    \end{minipage}%
  \end{frame}

  \begin{frame}{\textit{epw.x} --- Electron-phonon coupling using Wannier functions}
    {$\blacktriangleright$ mpirun -np 20 epw.x -nk 10 < 4\_epw.in > 4\_epw.out}
    \begin{minipage}{0.4\textwidth}
      \lstinputlisting[lastline=18]{../4_coupling/4_epw.in}
    \end{minipage}%
    \begin{minipage}{0.6\textwidth}
      \lstinputlisting[firstline=19, lastline=31]{../4_coupling/4_epw.in}
    \end{minipage}%
  \end{frame}

  \begin{frame}{\textit{epw.x} --- Electron-phonon coupling using Wannier functions}
    {$\blacktriangleright$ python 5\_plot.py}
    \lstinputlisting[language=python]{../4_coupling/5_plot.py}
    \popup{fig/2-6.pdf}
  \end{frame}

  \begin{frame}{\textit{epw.x} --- Electron-phonon coupling using Wannier functions}
    {$\blacktriangleright$ python 6\_plot.py}
    \lstinputlisting[language=python, lastline=21]{../4_coupling/6_plot.py}
    \popup{fig/4-6.pdf}
  \end{frame}
\end{document}
