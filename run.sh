#!/bin/bash

# 1. Apply "pw.x" and "bands.x":

cd 1_electrons
./run.sh
cd ..

# 2. Apply "pw2wannier90.x" and "wannier90.x":

cd 2_wannier
./run.sh
cd ..

# 3. Apply "ph.x", "q2r.x" and "matdyn.x":

cd 3_phonons
./run.sh
cd ..

# 4. Apply "epw.x":

cd 4_coupling
./run.sh
cd ..
