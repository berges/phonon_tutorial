#!/bin/bash

module load intel/2016
module load openmpi/2.0
module load quantum-espresso/6.3

WORK=/scratch/qm3grk
CONDA=$WORK/miniconda3/bin
export PYTHONPATH=$WORK/elphmod

NP=20 # number of processes
NK=10 # number of k-point pools
