#!/usr/bin/env python

import numpy as np
import matplotlib
matplotlib.use('tkagg')
import matplotlib.pyplot as plt
import elphmod

k, eps = elphmod.el.read_bands_plot('bands.dat.gnu', bands=13)

plt.plot(k, eps, color='red')
plt.xlabel('wave vector ($2 \pi / a$)')
plt.ylabel('electron energy (eV)')
plt.show()
