#!/bin/bash

. ../environment.sh

# 1. Perform self-consistent (SCF) DFT calculation:

mpirun -np $NP pw.x -nk $NK < 1_scf.in | tee 1_scf.out

# 2. Perform non-SCF DFT calculation:

mpirun -np $NP pw.x -nk $NK < 2_nscf.in | tee 2_nscf.out

# 3. Plot bands:

mpirun -np $NP bands.x -nk $NK < 3_bands.in | tee 3_bands.out

# 4. Show plotted bands:

$CONDA/python 4_plot.py
